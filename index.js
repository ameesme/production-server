// Native Modules
const http = require('http');
const https = require('https');

// Dependencies
const morgan = require('morgan');

// Modules
const secureConfig = require(`${__dirname}/modules/secureConfig.js`);
const app = require(`${__dirname}/modules/applicationConfig.js`);

// Instances
const logger = morgan('combined');

// Functions
function redirectToHttps(request, response) {
    logger(request, response, function (err) {
        const currentHost = request.headers.host;
        const currentUrl = request.url;
        response.writeHead(301, {"Location": "https://" + currentHost + currentUrl});
        response.end();
    });
}

// HTTP-server
const httpServer = http.createServer(redirectToHttps);
httpServer.listen(80,function(err){
    if (err) {
        console.log(`${new Date()} Failed to spawn HTTP-server`);
        throw new Error(err);
    }
    console.log(`${new Date()} HTTP-server started`);
});

// HTTPS-server
const credentials = {
    SNICallback: function (domain, callBack) {
        callBack(null, secureConfig.domainMap[domain]);
    },
    key: secureConfig.fallBack.key,
    cert: secureConfig.fallBack.cert,
    ca: secureConfig.fallBack.ca
};

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(443,function(err){
    if (err) {
        console.log(`${new Date()} Failed to spawn HTTPS-server`);
        throw new Error(err);
    }
    console.log(`${new Date()} HTTPS-server started`);
});
