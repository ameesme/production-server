// Dependencies
const express = require('express');
const vhost = require('vhost');
const morgan = require('morgan');
const helmet = require('helmet');
const compression = require('compression');
const minify = require('express-minify');

// Express
const application = express();

// Middleware
const logger = morgan('combined');
const OneYearInSeconds = 31536000;

application.use(logger);
application.use(compression());
application.use(minify());
application.use(helmet({
	hsts: {
		maxAge: OneYearInSeconds,
		includeSubDomains: true,
		preload: true
	},
	referrerPolicy: {
		policy: 'no-referrer'
	},
	contentSecurityPolicy: {
		directives: {
			defaultSrc: ["'self'"],
			scriptSrc: ["'self'", "'unsafe-inline'"],
		}
	}
}));

// Site modules
const ameesmeModule = require('../sites/amees.me/index');

// Sites
const sites = {
	ameesme: function () {
		return ameesmeModule;
	},
	ameesmeRedirect: function () {
		return function (request, response) {
			response.redirect(302, 'https://amees.me/');
		};
	}
};

// Vhosts
// amees.me
application.use(vhost('amees.me', sites.ameesme()));
application.use(vhost('www.amees.me', sites.ameesme()));

// meesboeijen.nl / meesboeijen.com
application.use(vhost('meesboeijen.nl', sites.ameesmeRedirect()));
application.use(vhost('www.meesboeijen.nl', sites.ameesmeRedirect()));
application.use(vhost('meesboeijen.com', sites.ameesmeRedirect()));
application.use(vhost('www.meesboeijen.com', sites.ameesmeRedirect()));

// Export module
module.exports = application;
