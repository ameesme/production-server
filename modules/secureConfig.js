// Native Modules
var tls = require('tls');
var fs = require('fs');

// TLS-contexts
const secureContexts = {
	ameesmeBase: tls.createSecureContext({
		key: fs.readFileSync('/etc/letsencrypt/live/amees.me/privkey.pem', 'utf8'),
		cert: fs.readFileSync('/etc/letsencrypt/live/amees.me/cert.pem', 'utf8'),
		ca: fs.readFileSync('/etc/letsencrypt/live/amees.me/chain.pem', 'utf8')
	}).context,
	meesboeijennlBase: tls.createSecureContext({
		key: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.nl/privkey.pem', 'utf8'),
		cert: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.nl/cert.pem', 'utf8'),
		ca: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.nl/chain.pem', 'utf8')
	}).context,
	meesboeijencomBase: tls.createSecureContext({
		key: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.com/privkey.pem', 'utf8'),
		cert: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.com/cert.pem', 'utf8'),
		ca: fs.readFileSync('/etc/letsencrypt/live/meesboeijen.com/chain.pem', 'utf8')
	}).context,
};

// Domain map to contexts
const domainMap = {
	'amees.me': secureContexts.ameesmeBase,
	'www.amees.me': secureContexts.ameesmeBase,
	'meesboeijen.nl': secureContexts.meesboeijennlBase,
	'www.meesboeijen.nl': secureContexts.meesboeijennlBase,
	'meesboeijen.com': secureContexts.meesboeijencomBase,
	'www.meesboeijen.com': secureContexts.meesboeijencomBase
};

// Fallback certificate (amees.me)
const fallBack = {
	key: fs.readFileSync('/etc/letsencrypt/live/amees.me/privkey.pem', 'utf8'),
	cert: fs.readFileSync('/etc/letsencrypt/live/amees.me/cert.pem', 'utf8'),
	ca: fs.readFileSync('/etc/letsencrypt/live/amees.me/chain.pem', 'utf8')
};

// Export module
module.exports = {
	domainMap,
	fallBack
};
